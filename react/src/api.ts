import axios from "axios";

const baseURL = "/";

export const authApi = () => axios.create({
  baseURL,
  headers: {
    Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
  },
});

export const api = () => axios.create({
  baseURL,
});
