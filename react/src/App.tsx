import React from "react";
import { Route } from "react-router";
import { BrowserRouter } from "react-router-dom";
import Home from "./components/Home";
import "./App.css";
import Sigin from "./components/Signin";
import Signup from "./components/Signup";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Route exact path="/login" component={Sigin} />
        <Route exact path="/register" component={Signup} />
        <Route exact path="/" component={Home} />
      </BrowserRouter>
    </div>
  );
}

export default App;
