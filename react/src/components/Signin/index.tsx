import React, { useState } from "react";
import { Button, Form, Container } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { api } from "../../api";
import "react-toastify/dist/ReactToastify.css";
const Sigin = () => {
  const history = useHistory();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const signIn = (event: React.FormEvent) => {
    event.preventDefault();
    api()
      .post("/signin", {
        username: email,
        password: password,
      })
      .then((res) => {
        localStorage.setItem("id", res.data.userId);
        localStorage.setItem("accessToken", res.data.accessToken);
        if (res.status === 200) {
          history.push("/");
          toast("You are successfully logged in!");
        }
      })
      .catch((err) => {
        if (err.response.status === 404) {
          toast("Wrong username or password!!");
        }
      });
  };

  return (
    <div>
      <h1>LOGIN</h1>
      <Container style={{ width: "100%" }}>
        <Form onSubmit={signIn}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              onChange={(event) => setEmail(event.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(event) => setPassword(event.target.value)}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Login
          </Button>
        </Form>
      </Container>
      <ToastContainer />
    </div>
  );
};

export default Sigin;
