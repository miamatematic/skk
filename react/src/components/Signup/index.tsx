import React, { useState } from "react";
import { Button, Form, Container } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import { api } from "../../api";
import "react-toastify/dist/ReactToastify.css";

const Signup = () => {
  const history = useHistory();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const signup = (event: React.FormEvent) => {
    event.preventDefault();
    api()
      .post("/signup", {
        userFirstName: firstName,
        userLastName: lastName,
        username: email,
        password: password,
      })
      .then((res) => {
        if (res.status === 201) {
          history.push("/login");
          toast("You are succesfully signed up!");
        }
      })
      .catch((err) => {
        if (err.response.status === 400) {
          toast("Some fields are empty.");
        } else if (err.response.status === 409) {
          toast("Username already exists!");
        } else {
          toast("Error while signing up!");
        }
      });
  };

  return (
    <div>
      <h1>REGISTER</h1>
      <Container style={{ width: "50%" }}>
        <Form onSubmit={signup}>
          <Form.Group controlId="formFirstName">
            <Form.Label>First name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter first name"
              onChange={(event) => setFirstName(event.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formLastName">
            <Form.Label>Last name</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter last name"
              onChange={(event) => setLastName(event.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              onChange={(event) => setEmail(event.target.value)}
            />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              onChange={(event) => setPassword(event.target.value)}
            />
          </Form.Group>
          <Button variant="primary" type="submit">
            Register
          </Button>
        </Form>
      </Container>
      <ToastContainer />
    </div>
  );
};

export default Signup;
