import React, { useState, useEffect } from "react";
import moment from "moment";
import { Table, Button, Container, Modal, Form } from "react-bootstrap";
import { Journey } from "../../types/Journey";
import { Transaction } from "../../types/Transaction";
import { User } from "../../types/User";
import { isUserLoggedIn } from "../../utils/loggedIn";
import { ToastContainer, toast } from "react-toastify";
import { authApi, api } from "../../api";
import "react-toastify/dist/ReactToastify.css";

const HOURS_IN_MILISECONDS = 1 * 60 * 60 * 1000;

const Journeys = () => {
  const [journeys, setJourneys] = useState<Journey[]>([]);
  const [transactions, setTransactions] = useState<Transaction[]>([]);
  const [showTransactions, setShowTransactions] = useState(false);
  const [showBuyForm, setShowBuyForm] = useState(false);
  const [journeyToGo, setJourneyToGo] = useState<Journey>();
  const [cardNumber, setCardNumber] = useState<string>("");
  const [user, setUser] = useState<User>();
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    api()
      .get("/journey")
      .then((res) => {
        setJourneys(res.data);
      })
      .catch(() => {
        toast("Error fetching data");
      });
  }, [counter]);

  const buyTicket = (event: React.FormEvent) => {
    event.preventDefault();

    if (journeyToGo) {
      authApi()
        .put("/buy", {
          journeyId: journeyToGo._id,
          cardNumber: cardNumber,
        })
        .then((res) => {
          if (res.status === 200) {
            setCounter(counter + 1);
            setShowBuyForm(false);
            toast("Ticket bought successfully!");
          }
        })
        .catch((err) => {
          if (err.response.status === 409) {
            toast("Sorry, no tickets left!");
            setShowBuyForm(false);
          } else {
            toast("Error buying a ticket!");
          }
        });
    }
  };

  const cancelTicket = (transaction: Transaction) => {
    if (notOkToCancelTicket(transaction.departureTime)) {
      toast("Sorry, it is too late to cancel your ticket");
      return;
    }
    authApi()
      .put("/cancel", {
        journeyId: transaction.journeyId,
        transactionId: transaction.id,
      })
      .then((res) => {
        getTransactions();
        setCounter(counter + 1);
        toast("Ticket is successfully canceled!");
      })
      .catch(() => {
        toast("Error canceling ticket");
      });
  };

  const notOkToCancelTicket = (departureTime: Date) => {
    const departureMiliseconds = new Date(departureTime).getTime();
    const nowTime = Date.now();
    return (
      departureMiliseconds - nowTime < HOURS_IN_MILISECONDS ||
      departureMiliseconds - nowTime <= 0
    );
  };

  const getTransactions = () => {
    authApi()
      .get("/transaction")
      .then((res) => {
        setTransactions(res.data);
      })
      .catch(() => {
        toast("Error fetching transactions");
      });
  };

  const closeTransactionsModal = () => {
    setShowTransactions(false);
  };

  const onShowTransactions = () => {
    if (isUserLoggedIn()) {
      setShowTransactions(true);
      getTransactions();
    } else {
      toast("You have to be logged in to see your transactions!");
    }
  };

  const onBuyTicket = (journey: Journey) => {
    if (!isUserLoggedIn()) {
      toast("You have to be logged in to buy a ticket!");
    } else {
      setShowBuyForm(true);
      setJourneyToGo(journey);
      authApi()
        .get("/user")
        .then((res) => {
          setUser(res.data);
        })
        .catch(() => {
          toast("Error fetching user");
        });
    }
  };

  const convertDateTime = (dateTime: Date) =>
    moment(dateTime).format("DD-MM-YYYY kk:mm");

  return (
    <Container style={{ marginTop: "50px" }}>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>From</th>
            <th>To</th>
            <th>Departure time</th>
            <th>Transporter name</th>
            <th>Max num of seats</th>
            <th>Available seats</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {journeys.map((journey: Journey, index: number) => (
            <tr key={index}>
              <td>{journey.from}</td>
              <td>{journey.to}</td>
              <td>{convertDateTime(journey.departureTime)}</td>
              <td>{journey.transporterName}</td>
              <td>{journey.maxNumOfSeats}</td>
              <td>{journey.availableSeats}</td>
              <td>{journey.price}</td>
              <td>
                <Button onClick={() => onBuyTicket(journey)}>Buy ticket</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      <Button
        variant="outline-info"
        size="lg"
        block
        style={{ margin: "10px" }}
        onClick={onShowTransactions}
      >
        Show transactions
      </Button>
      <Modal show={showTransactions} onHide={closeTransactionsModal} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Transactions</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {transactions.length !== 0 ? (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Departure time</th>
                  <th>Transporter name</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                {transactions.map((transaction: Transaction, index: number) => (
                  <tr key={index}>
                    <td>{convertDateTime(transaction.date)}</td>
                    <td>{transaction.from}</td>
                    <td>{transaction.to}</td>
                    <td>{convertDateTime(transaction.departureTime)}</td>
                    <td>{transaction.transporterName}</td>
                    <td>{transaction.price}</td>
                    <td>
                      <Button onClick={() => cancelTicket(transaction)}>
                        {"Cancel ticket"}
                      </Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div>No transactions</div>
          )}
        </Modal.Body>
      </Modal>
      <Modal show={showBuyForm} onHide={() => setShowBuyForm(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Buy Ticket</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container style={{ width: "50%" }}>
            {user ? (
              <Form onSubmit={buyTicket}>
                <Form.Group controlId="formFirstName">
                  <Form.Label>First name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter first name"
                    defaultValue={user.firstName}
                  />
                </Form.Group>
                <Form.Group controlId="formLastName">
                  <Form.Label>Last name</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Enter last name"
                    defaultValue={user.lastName}
                  />
                </Form.Group>
                <Form.Group controlId="formCardNumber">
                  <Form.Label>Card Number</Form.Label>
                  <Form.Control
                    type="number"
                    placeholder="Enter card number"
                    required
                    onChange={(event) => setCardNumber(event.target.value)}
                  />
                </Form.Group>
                <Button variant="primary" type="submit">
                  Buy
                </Button>
              </Form>
            ) : (
              <></>
            )}
          </Container>
        </Modal.Body>
      </Modal>
      <ToastContainer />
    </Container>
  );
};

export default Journeys;
