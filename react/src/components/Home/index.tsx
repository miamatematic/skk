import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Button, Container } from "react-bootstrap";
import Journeys from "../Journeys";
import { isUserLoggedIn } from "../../utils/loggedIn";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Home = () => {
  const [loggedIn, setLoggedIn] = useState<boolean>(false);

  const accessToken = localStorage.getItem("accessToken");
  useEffect(() => {
    setLoggedIn(isUserLoggedIn);
  }, [accessToken]);

  const signOut = () => {
    localStorage.clear();
    setLoggedIn(false);
    toast("You are signed out");
  };

  return (
    <div>
      <Container style={{ width: "30%" }}>
        {loggedIn ? (
          <Button
            variant="outline-info"
            size="lg"
            block
            style={{ margin: "10px" }}
            onClick={signOut}
          >
            Log out
          </Button>
        ) : (
          <div>
            <Link to="/register">
              <Button
                variant="outline-info"
                size="lg"
                block
                style={{ margin: "10px" }}
              >
                Register
              </Button>
            </Link>
            <Link to="/login">
              <Button
                variant="outline-info"
                size="lg"
                block
                style={{ margin: "10px" }}
              >
                Login
              </Button>
            </Link>
          </div>
        )}
      </Container>
      <Container style={{ width: "80%" }}>
        <Journeys />
      </Container>
      <ToastContainer />
    </div>
  );
};

export default Home;
