export interface Journey {
  _id: string;
  from: string;
  to: string;
  departureTime: Date;
  transporterName: string;
  availableSeats: number;
  price: string;
  maxNumOfSeats: number;
}