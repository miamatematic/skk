export interface Transaction {
  journeyId: string;
  from: string;
  to: string;
  departureTime: Date;
  date: Date,
  transporterName: string;
  price: string;
  id: string;
}