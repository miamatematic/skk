const mongoose = require("mongoose");
const Journey = require("../schemas/journey.schema");

const initMongo = () => {
  // Hostala sam vlastiti mongo za lakse pokretanje. Slobodno zamijeniti sa lokalnom instancom.
  mongoose.connect(
    "mongodb+srv://mia:pass@skk-0reaf.mongodb.net/skk?retryWrites=true&w=majority",
    { useNewUrlParser: true },
    async (err) => {
      if (err) {
        console.error(err);
      } else {
        console.info("Mongodb is connected.");
        try {
          const journeys = await Journey.find({});
          if (journeys.length === 0) {
            initData();
          }
        } catch (error) {
          res.sendStatus(500);
        }
      }
    }
  );
};

const initData = () => {
  const journey1 = new Journey({
    departureTime: "2020-08-25T12:00:36.000+00:00",
    transporterName: "A.P.Rijeka",
    from: "Zagreb",
    to: "Rijeka",
    availableSeats: 10,
    maxNumOfSeats: 10,
    price: "50 kn",
  });
  journey1.save();
  const journey2 = new Journey({
    departureTime: "2020-09-26T16:00:36.000+00:00",
    transporterName: "A.P.Pula",
    from: "Zagreb",
    to: "Pula",
    availableSeats: 5,
    maxNumOfSeats: 5,
    price: "60 kn",
  });
  journey2.save();
  const journey3 = new Journey({
    departureTime: "2020-09-26T14:00:36.000+00:00",
    transporterName: "A.P.Sisak",
    from: "Sisak",
    to: "Split",
    availableSeats: 4,
    maxNumOfSeats: 4,
    price: "150 kn",
  });
  journey3.save();
  const journey4 = new Journey({
    departureTime: "2020-08-25T17:00:36.000+00:00",
    transporterName: "A.P.Dubrovnik",
    from: "Dubrovnik",
    to: "Zadar",
    availableSeats: 3,
    maxNumOfSeats: 3,
    price: "50 kn",
  });
  journey4.save();
};

module.exports = initMongo;
