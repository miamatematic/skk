const jwt = require("jsonwebtoken");

const User = require("../schemas/user.schema");

const accesTokenSecret = require("../utils/constants");

const authenticate = (req, res, next) => {
  try {
    const authHeader = req.headers.authorization;

    if (authHeader) {
      const token = authHeader.split(" ")[1];

      jwt.verify(token, accesTokenSecret, async (err, user) => {
        if (err) {
          return res.sendStatus(403);
        }

        _user = await User.findById(user.id);

        if (!_user) {
          return res.sendStatus(401);
        }

        req.user = _user;

        next();
      });
    } else {
      res.sendStatus(401);
    }
  } catch (err) {
    return res.sendStatus(500);
  }
};

module.exports = {
  authenticate,
};
