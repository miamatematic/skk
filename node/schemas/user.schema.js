const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  firstName: String,
  lastName: String,
  username: String,
  password: String,
});

module.exports = model("users", userSchema);
