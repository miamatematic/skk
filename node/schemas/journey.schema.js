const { Schema, model} = require('mongoose');

const journeySchema = new Schema({
  from: String,
  to: String,
  departureTime: Date,
  transporterName: String,
  availableSeats: Number,
  maxNumOfSeats: Number,
  price: String
})

module.exports = model('journeys', journeySchema);