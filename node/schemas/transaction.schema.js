const { Schema, model } = require('mongoose');

const transactionSchema = new Schema({
  userId: String,
  journeyId: String,
  date: Date,
});

module.exports = model('transactions', transactionSchema);