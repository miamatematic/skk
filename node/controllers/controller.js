const jwt = require("jsonwebtoken");
const User = require("../schemas/user.schema");
const Transaction = require("../schemas/transaction.schema");
const Journey = require("../schemas/journey.schema");

const accessTokenSecret = require("../utils/constants");

const home = (req, res) => {
  return res.sendStatus(200);
};

const signup = async (req, res) => {
  try {
    const { userFirstName, userLastName, username, password } = req.body;

    if (!userFirstName || !userLastName || !username || !password) {
      return res.sendStatus(400);
    }

    const dbUser = await User.find({ username: username });
    if (dbUser.length !== 0) {
      return res.sendStatus(409);
    }

    const newUser = new User({
      firstName: userFirstName,
      lastName: userLastName,
      username: username,
      password: password,
    });

    await newUser.save();

    res.sendStatus(201);
  } catch (error) {
    res.sendStatus(500);
  }
};

const signin = async (req, res) => {
  try {
    const { username, password } = req.body;

    const dbUser = await User.findOne({ username: username });
    if (!dbUser || dbUser.password !== password) {
      return res.sendStatus(404);
    }

    const encoded = jwt.sign({ id: dbUser._id }, accessTokenSecret);
    const auth = { userId: dbUser._id, accessToken: encoded };

    res.status(200).send(auth);
  } catch (error) {
    res.sendStatus(500);
  }
};

const getJourneys = async (req, res) => {
  try {
    const journeys = await Journey.find({});

    res.status(200).json(journeys);
  } catch (error) {
    res.sendStatus(500);
  }
};

const buyTicket = async (req, res) => {
  try {
    const {
      user,
      body: { journeyId, cardNumber },
    } = req;

    if (!journeyId || !cardNumber) {
      res.sendStatus(400);
    }

    const dbJourney = await Journey.findById(journeyId);
    if (!dbJourney) {
      return res.sendStatus(400);
    }

    if (dbJourney.availableSeats === 0) {
      return res.sendStatus(409);
    }
    dbJourney.availableSeats = dbJourney.availableSeats - 1;
    await dbJourney.save();

    const newTransaction = new Transaction({
      userId: user._id,
      journeyId: journeyId,
      date: new Date(),
    });
    await newTransaction.save();

    res.status(200).json(dbJourney);
  } catch (error) {
    res.sendStatus(500);
  }
};

const cancelTicket = async (req, res) => {
  try {
    const { journeyId, transactionId } = req.body;

    const dbJourney = await Journey.findById(journeyId);
    const transactionToDelete = await Transaction.findById(transactionId);

    if (!dbJourney || !transactionToDelete) {
      return res.sendStatus(400);
    }

    dbJourney.availableSeats = dbJourney.availableSeats + 1;
    await dbJourney.save();

    await transactionToDelete.remove();

    res.sendStatus(200);
  } catch (error) {
    res.sendStatus(500);
  }
};

const getTransactionsByUser = async (req, res) => {
  try {
    const { user } = req;

    const transactions = await Transaction.find({ userId: user._id });

    if (transactions.length === 0) {
      return res.sendStatus(204);
    }

    const responseData = await Promise.all(
      transactions.map(async (transaction) => {
        const journey = await Journey.findById(transaction.journeyId);
        return {
          from: journey.from,
          to: journey.to,
          departureTime: journey.departureTime,
          date: transaction.date,
          transporterName: journey.transporterName,
          price: journey.price,
          journeyId: journey._id,
          id: transaction._id,
        };
      })
    );

    res.status(200).json(responseData);
  } catch (error) {
    res.sendStatus(500);
  }
};

const getUser = async (req, res) => {
  try {
    const { user } = req;

    res.status(200).json({
      firstName: user.firstName,
      lastName: user.lastName,
      username: user.username,
    });
  } catch (error) {
    res.sendStatus(500);
  }
};

module.exports = {
  home,
  signup,
  signin,
  getJourneys,
  buyTicket,
  cancelTicket,
  getTransactionsByUser,
  getUser,
};
