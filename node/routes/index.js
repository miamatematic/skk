const express = require("express");
const router = express.Router();
const cors = require("cors");

router.use(cors());

const controller = require("../controllers/controller");
const authMiddleware = require("../middlewares/auth");

router.get("/", controller.home);

router.post("/signup", controller.signup);

router.get("/journey", controller.getJourneys);
router.post("/signin", controller.signin);
router.put("/buy", authMiddleware.authenticate, controller.buyTicket);
router.put("/cancel", authMiddleware.authenticate, controller.cancelTicket);
router.get("/transaction", authMiddleware.authenticate, controller.getTransactionsByUser);
router.get("/user", authMiddleware.authenticate, controller.getUser);

module.exports = router;
